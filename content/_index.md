### Get Fedilab
<div class='row'>
    <div class='col-sm-6'>
    <a href='https://f-droid.org/app/fr.gouv.etalab.mastodon'><img alt='Get it on Fdroid' src='fdroid.png' height='100'/></a>
    </div>
      <div class='col-sm-6'>
         <a href='https://play.google.com/store/apps/details?id=app.fedilab.android'><img alt='Get it on Google Play' src='play.png' height='100'/></a>
    </div>
</div>
<br/>
Project:  <a href='https://framagit.org/tom79/fedilab'>https://framagit.org/tom79/fedilab</a>
<br/>

<center><h2>All other apps</h2></center>


<h3 id='NitterizeMe'>UntrackMe</h3>
<i>Transform Youtube and Twitter links into Invidious and Nitter links</i>
<br><br>
<div class='row'>
    <div class='col-sm-6'>
    Full links version <img  style="display:inline;" src="./logos/untrackme.png" width="40px"><br/>
     <a href='https://f-droid.org/app/app.fedilab.nitterizeme'><img alt='Get it on Fdroid' src='fdroid.png' height='100'/></a>
    </div>
     Lite version <img  style="display:inline;" src="./logos/untrackme_lite.png" width="40px"><br/>
        <div class='col-sm-6'>
         <a href='https://f-droid.org/app/app.fedilab.nitterizemelite'><img alt='Get it on Fdroid' src='fdroid.png' height='100'/></a>
    </div>
</div>
<br/>
Project:  <a href='https://framagit.org/tom79/nitterizeme'>https://framagit.org/tom79/nitterizeme</a>
<hr/>
<h3 id='TubeLab'>TubeLab <img  style="display:inline;" src="./logos/tubelab.png" width="40px"></h3>
<i>Full featured Peertube Android app</i>
<br><br>
<div class='row'>
    <div class='col-sm-6'>
    <a href='https://f-droid.org/packages/app.fedilab.tubelab/'><img alt='Get it on Fdroid' src='fdroid.png' height='100'/></a>
    </div>
      <div class='col-sm-6'>
         <a href='https://play.google.com/store/apps/details?id=app.fedilab.tubelab'><img alt='Get it on Google Play' src='play.png' height='100'/></a>
    </div>
</div>
<br/>
Project:  <a href='https://framagit.org/tom79/fedilab-tube'>https://framagit.org/tom79/fedilab-tube</a>
<hr/>
<h3 id='TubeAcad'>TubeAcad <img  style="display:inline;" src="./logos/tubeacad.png" width="40px"></h3>
<i>Peertube Android app for French Academic instances</i>
<br><br>
<div class='row'>
       <div class='col-sm-6'>
       <a href='https://f-droid.org/packages/app.fedilab.fedilabtube/'><img alt='Get it on Fdroid' src='fdroid.png' height='100'/></a>
       </div>
         <div class='col-sm-6'>
            <a href='https://play.google.com/store/apps/details?id=app.fedilab.fedilabtube'><img alt='Get it on Google Play' src='play.png' height='100'/></a>
       </div>
</div>
<br/>
Project:  <a href='https://framagit.org/tom79/fedilab-tube'>https://framagit.org/tom79/fedilab-tube</a>
<hr/>
<h3 id='OpenMultiMaps'>OpenMultiMaps <img  style="display:inline;" src="./logos/openmultimaps.png" width="40px"></h3>
<i>A simple client to display maps from OpenStreetMap.</i>
<br><br>
<div class='row'>
       <div class='col-sm-6'>
       <a href='https://f-droid.org/packages/app.fedilab.openmaps/'><img alt='Get it on Fdroid' src='fdroid.png' height='100'/></a>
       </div>
</div>
<br/>
Project:  <a href='https://framagit.org/tom79/openmaps'>https://framagit.org/tom79/openmaps</a>
<hr/>
<h3 id='FediPlan'>FediPlan</h3>
<i>Open source web application for scheduling with Mastodon or Pleroma</i>
<br><br>
<div class='row'>
    <div class='col-sm-12'>
    <a href='./page/fediplan'><img alt='FediPlan' src='fediplan.png'  height='100'/></a>
    </div>
</div>
Project:  <a href='https://framagit.org/tom79/fediplan'>https://framagit.org/tom79/fediplan</a>
<hr/>
