DISCLAIMER: This tutorial is more suitable for experienced users. Proceed at your own risk!. If you're not sure about this, please use another method to get Fedilab ([Installation Methods](../../install))

Steps provided here are to be used in a Debian/Ubuntu based distributions. For other distributions and operating systems, some of them might be different

#### Install Java JDK, Git & unzip (First time only)

`sudo apt-get install openjdk-8-jdk-headless git unzip`

**JDK** (Java Development Kit) is necessary to build Android apps.<br>
**Git** is the program we use to sync Fedilab's repository to your PC.<br>
**Unzip** will help us unpack the Android SDK zip file.

#### Get Android SDK (First time only)

Android SDK is required for apps to get their dependencies. We don't need the whole Android Studio program, but just the command line tools. The link in the below command is the current version, but to check if a newer version is available go [HERE](https://developer.android.com/studio/#command-tools).

 - Download SDK<br>
 `wget https://dl.google.com/android/repository/sdk-tools-linux-4333796.zip -O sdk.zip`

 - Extract it<br>
 `unzip sdk.zip -d android_sdk/`

 - Accept SDK licenses
  - Go to this folder<br>
  `cd android_sdk/tools/bin/`

  - Then run this and answer yes when asked to accept licenses<br>
  `./sdkmanager --licenses`

#### Setup environment (First time only)
To make things work, now you have to set up ANDROID_HOME environment variable.

- To do that add this line to end of your **.bashrc** file.<br>
*This file usually can be found in user's home directory.*<br><br>
`export ANDROID_HOME=/home/<username>/<pathto>/android_sdk`<br><br>
*\*Replace `<username>` and `<pathto>` with the proper values*

- Then run this command to reload the changes<br>
`source .bashrc`

#### Create your signature (First time only)
To install Fedilab to your android you need to sign it with a signature. Now it's time to create your signature!<br>
This will create a file with .keystore extension which will contain your signature. So keep that file safe and remember the password

- Create Keystore<br>
`keytool -genkey -v -keystore YOURALIAS.keystore -keyalg RSA -keysize 4096 -validity 10000 -alias YOURALIAS`<br><br>
*\*Replace* `YOURALIAS` *with your name*

- Then answer the questions it ask. Some of them are optional and can be left blank.<br>This command might give you a warning, let's fix it with this command!

- Move keystore to PKCS12<br>
`keytool -importkeystore -srckeystore YOURALIAS.keystore -destkeystore YOURALIAS.keystore -deststoretype pkcs12`<br><br>


#### Building Fedilab the first time:

- Clone Fedilab's repository<br>
`git clone https://framagit.org/tom79/fedilab.git fedilab`

- Go to the cloned folder<br>
`cd fedilab`

- **Build!**<br>
`./gradlew assembleFdroid`

#### Updating the app (not the first time)

- Go to the Fedilab repository folder<br>
`cd fedilab`

- Get the latest code<br>
`git pull`

- **Build!**<br>
`./gradlew assembleFdroid`

#### Sign the app
Now it's time to sign the app you just built

- Go to the directory where the apk files you built are stored<br>
`cd app/build/outputs/apk/fdroid/release/`

- Rename the apk file<br>
`mv app-fdroid-release-unsigned.apk fedilab.apk`

- Sign the app<br>
`/home/<username>/<pathto>/android_sdk/build-tools/28.0.3/apksigner sign --ks <pathto>/YOURALIAS.keystore --ks-key-alias YOURALIAS fedilab.apk`<br><br>
*\*Replace* `<username>`*,* `<pathto>` *and* `YOURALIAS` *with the proper values*

- Then enter the password when asked


#### Install
And you're done! Move the fedilab.apk file which you just signed app to your phone, and install it. If you already have Fedilab installed from F-Droid or Google Play, you have to uninstall that first.<br>
To prevent data loss when uninstalling, you can use Fedilab's data export/import feature. Read more about [Export/Import Feature](../features/export-import-data)<br><br><br><br>


#### Steps to make things easier (optional)<br><br>

- In order to avoid having to type long codes every time you sign an app, add an alias for apksigner to the end of your .bashrc file.<br><br>
`alias apksigner=/home/<username>/<pathto>/android_sdk/build-tools/28.0.3/apksigner sign --ks <pathto>/YOURALIAS.keystore --ks-key-alias YOURALIAS`<br><br>
*\*Replace* `<username>`*,* `<pathto>` *and* `YOURALIAS` *with the proper values*

- This new alias will be available when you login next time. Or you can run the below command to apply the changes right now<br>
`source .bashrc`

- After adding this alias, you can sign apps easily with just this command<br>
`apksigner fedilab.apk`<br><br>



**This tutorial is based on [THIS](https://fediverse.blog/~/SylkeViciousThinks/how-to-build-an-android-app-from-source/) article by [Sylke Vicious](https://ins.mastalab.app/@metalbiker)**
